#!/bin/bash
EXIT_CODE=0
function determineExitCode {
    docker-compose -f $1 ps -q | xargs docker inspect -f '{{ .State.ExitCode }}' | while read code; do
        if [ "$code" == "1" ]; then
           echo "$1 failed"
           EXIT_CODE+=1
        fi
    done
}

function build {
    echo "$1"
    docker-compose -f $1 rm -f
    docker-compose -f $1 up --build
}

## MAIN PROGRAM ##
build $1
determineExitCode $1
exit ${EXIT_CODE}