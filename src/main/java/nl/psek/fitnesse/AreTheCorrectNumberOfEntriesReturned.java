package nl.psek.fitnesse;

import io.restassured.path.json.JsonPath;
import io.restassured.path.json.config.JsonPathConfig;
import io.restassured.response.Response;

import java.util.List;

import static io.restassured.RestAssured.given;
import static io.restassured.http.ContentType.JSON;

/**
 * Created by Pascal Smeets on 19-02-2018.
 */
public class AreTheCorrectNumberOfEntriesReturned {

    private String path;
    private int entriesReturned;
    private int entriesExpected;

    private Response response = null;
    private String baseURL = "https://jsonplaceholder.typicode.com/";

    public void setPath(String path) {
        this.path = path;
    }

    public void setEntriesExpected(int entriesExpected) {
        this.entriesExpected = entriesExpected;
    }

    public String isTheNumberOfEntriesCorrect() {
        return this.entriesExpected == this.entriesReturned ? "yes" : "no";

    }

    public int whatAreTheNumberOfEntriesReturned() {
        return this.entriesReturned;
    }

    // The following functions are optional.  If they aren't declared they'll be ignored.
    public void execute() {

        this.response = given().get(this.baseURL + this.path)
                .then().contentType(JSON)
                .extract().response();

        String json = response.getBody().asString();

        System.out.println("Rest call using path = " + this.path);
        System.out.println("JSON response = " + json);

        JsonPath jsonPath = new JsonPath(json).using(new JsonPathConfig("UTF-8")).setRoot("");

        List<String> listOfOfEntriesReturned = jsonPath.get("id");

        System.out.println("JSON lijst van entries = " + listOfOfEntriesReturned.toString());
        System.out.println("JSON grootte lijst van entries =  " + listOfOfEntriesReturned.size());
        this.entriesReturned = listOfOfEntriesReturned.size();

    }

    public void reset() {
    }

    public void table(List<List<String>> table) {
    }

    public void beginTable() {
    }

    public void endTable() {
    }

}

