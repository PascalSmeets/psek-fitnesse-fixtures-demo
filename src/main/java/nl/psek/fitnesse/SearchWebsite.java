package nl.psek.fitnesse;

import nl.psek.fitnesse.fixtures.selenium.SlimWebDriver;

/**
 * Created by pascal on 10-05-2017.
 */
public class SearchWebsite {

    private static String url = "http://www.kvk.nl";
    private static String xpathZoekenButton = "//button[text()='Zoeken']";
    private static String xpathInputField = "//input";
    private static String noResultsText = "Helaas, er zijn geen resultaten voor uw zoekopdracht";

    public boolean checkThatThereIsNoSearchResultFor(String input) throws InterruptedException {
        SlimWebDriver fixture = new SlimWebDriver("selenium.properties");

        fixture.defaultCommandTimeout(5);
        fixture.defaultPageLoadTimeout(10);

        fixture.openWebsite(url);
        fixture.type(xpathInputField, input);
        fixture.click(xpathZoekenButton);
        Thread.sleep(2000);
        return fixture.verifyTextPresent(noResultsText);

    }

}
