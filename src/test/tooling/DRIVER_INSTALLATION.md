# How to install the browser specific webdriver drivers

### Installation of Mozilla Geckodriver

Download the latest version from [https://github.com/mozilla/geckodriver/releases](https://github.com/mozilla/geckodriver/releases)

### Installation of ChromeDriver

Download the latest version from [https://chromedriver.chromium.org](https://chromedriver.chromium.org)

### make it availible with setDriverLocation(String propertyname, String path_to_driverfile)

an example for ChromeDriver

|setDriverLocation;|webdriver.chrome.driver|src/test/tooling/chromedriver.exe|   (windows)
|setDriverLocation;|webdriver.chrome.driver|src/test/tooling/chromedriver    |   (mac / linux)

an example for Geckodriver

|setDriverLocation;|webdriver.gecko.driver|src/test/tooling/geckodriver.exe|   (windows)
|setDriverLocation;|webdriver.gecko.driver|src/test/tooling/geckodriver    |   (mac / linux)