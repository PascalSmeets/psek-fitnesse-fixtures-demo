!*> Algemene Scenario's
!-
    <b>Let Op:</b><br/>
    <br/>
    <b>Maak geen wijzigingen in deze of één van de hieronder staande bestanden / mappen.</b>

    Wijzigingen dienen gemaakt te worden in de ps-fitnesse-archetype module zodat deze
    breed ingezet kunnen worden.
-!
*!

!4 General available scenarios:
!include -c .ScenarioLibrary.WebApplication
